Rails.application.routes.draw do
  get 'reservations/create'

  get 'users/show'

  devise_for :users, :path => '',
                    path_names: { :sign_in => 'Login', :sign_out => 'logout', :edit => 'profile'},
                    controllers: { registrations: :registrations,
                                   confirmations: :confirmations
                     }
  root 'pages#home'

  resources :users , only: [ :show ]
  resources :rooms, path: 'listings' do
    resources :reservations, only: [ :create ]
    resources :reviews, only: [:create, :destroy]
  end
  resources :photos

  resources :conversations, only: [:index, :create] do
    resources :messages, only: [:index, :create]
  end

  get '/preload', to: 'reservations#preload'
  get '/preview', to: 'reservations#preview'
  get 'your_trips', to: "reservations#your_trips", path: 'my_trips'
  get '/your_reservations', to: 'reservations#your_reservations', path: 'my_reservations'

  get '/search', to: 'pages#search'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
